package com.example.livetouch.ui.project.Controller;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Parcelable;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.livetouch.R;
import com.example.livetouch.ui.project.model.AssyncPOST;
import com.example.livetouch.ui.project.model.User;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.concurrent.ExecutionException;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class MainActivity extends AppCompatActivity {

    private Button b1;
    private EditText etLogin;
    private EditText etPassword;

    private static final String ARQUIVO_PREFERENCIA = "ArquivoPreferencia";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        etLogin = (EditText)findViewById(R.id.etLogin);
        etPassword = (EditText)findViewById(R.id.etSenha);
        b1 = (Button)findViewById(R.id.btnLogin);

        b1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String login = etLogin.getText().toString();
                String senh = etPassword.getText().toString();
                User user = new User();
                AssyncPOST syncpost = new AssyncPOST();
                syncpost.setLogin(login);
                syncpost.setSenha(senh);
                try {
                    user = syncpost.execute().get();
                } catch (ExecutionException e) {
                    e.printStackTrace();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                if (user.getToken() != null || user.getToken() != ""){
                    Intent intent = new Intent(MainActivity.this,drawCars.class);
                    intent.putExtra("usuario", user);
                    startActivity(intent);
                }
                else
                    Toast.makeText(getApplicationContext(),"login ou senha incorretos",Toast.LENGTH_LONG);
//                if (etLogin.getText().toString().equals("user") && etPassword.getText().toString().equals("123")){
//                    Intent intent = new Intent(MainActivity.this,drawCars.class);
//                    intent.putExtra("nome", "Admin");
//                    intent.putExtra("email", "admin@gmail.com");
//
//
//                    SharedPreferences sharedPreferences = getSharedPreferences(ARQUIVO_PREFERENCIA,0);
//                    SharedPreferences .Editor editor = sharedPreferences.edit();
//                    editor.putString("login",login);
//                    editor.putString("senha",senh);
//                    editor.commit();
//
//
//                    startActivity(intent);
//                }
//                else
//                    Toast.makeText(getApplicationContext(),"login ou senha incorretos",Toast.LENGTH_LONG).show();
            }
        });

    }
}
