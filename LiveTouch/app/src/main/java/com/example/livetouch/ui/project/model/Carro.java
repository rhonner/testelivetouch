package com.example.livetouch.ui.project.model;

public class Carro {
    private String modelo;
    private int imagem;
    private String tipoCarro;

    public Carro(String modelo, String tipoCarro, int imagem){
        this.modelo = modelo;
        this.tipoCarro = tipoCarro;
        this.imagem = imagem;
    }

    public String getTipoCarro() {
        return tipoCarro;
    }

    public void setTipoCarro(String tipoCarro) {
        this.tipoCarro = tipoCarro;
    }

    public String getModelo() {
        return modelo;
    }

    public void setModelo(String modelo) {
        this.modelo = modelo;
    }

    public int getImagem() {
        return imagem;
    }

    public void setImagem(int imagem) {
        this.imagem = imagem;
    }
}
