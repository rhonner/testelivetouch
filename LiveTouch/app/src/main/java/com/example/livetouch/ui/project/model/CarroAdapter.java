package com.example.livetouch.ui.project.model;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.livetouch.R;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.List;

public class CarroAdapter extends ArrayAdapter<Carro> {

    private  Context context;
    private  List<Carro>elementos = null;

    public CarroAdapter(Context context, List<Carro> elementos){
        super(context,0, elementos);
        this.context = context;
        this.elementos = elementos;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent){
        Carro c = elementos.get(position);
        if (convertView == null){
            convertView = LayoutInflater.from(context).inflate(R.layout.linha, null);
        }
        ImageView carImg = (ImageView) convertView.findViewById(R.id.imagemCarro);
        carImg.setImageResource(c.getImagem());
        TextView carName = (TextView)convertView.findViewById(R.id.modelo);
        carName.setText(c.getModelo());
        return convertView;
//        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
//        View rowView = inflater.inflate(R.layout.linha,parent,false);
//
//        TextView modeloCarro = (TextView) rowView.findViewById(R.id.modelo);
//        TextView tipoCarro = (TextView) rowView.findViewById(R.id.tipoCarro);
//        ImageView imagem = (ImageView) rowView.findViewById(R.id.imagem);
//
//        modeloCarro.setText(elementos.get(position).getModelo());
//        tipoCarro.setText(elementos.get(position).getTipoCarro());
//        imagem.setImageResource(elementos.get(position).getImagem());
//
//        return rowView;


    }


    private ArrayList<Carro>adicionarCarros(){
        ArrayList<Carro>carros = new ArrayList<Carro>();

        Carro c = new Carro("gallardo","corrida",R.drawable.ferrari);
        carros.add(c);
        c = new Carro("astra","popular",R.drawable.astra);
        carros.add(c);
        c = new Carro("ferrari","corrida",R.drawable.gallardo);
        carros.add(c);
        c = new Carro("aventador","corrida",R.drawable.bugatti);
        carros.add(c);
        c = new Carro("bugatti","corrida",R.drawable.aventador);
        carros.add(c);

        return carros;
    }
}
