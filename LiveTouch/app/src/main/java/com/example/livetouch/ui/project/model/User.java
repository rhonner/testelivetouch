package com.example.livetouch.ui.project.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.io.Serializable;

public class User implements Serializable {
    int id;
    String login;
    String nome;
    String email;
    String urlFoto;
    String token;
    String[] roles;

    public User(){}

    public User(int id, String login, String nome, String email, String urlFoto, String token, String[] roles) {
        this.id = id;
        this.login = login;
        this.nome = nome;
        this.email = email;
        this.urlFoto = urlFoto;
        this.token = token;
        this.roles = roles;
    }

    protected User(Parcel in) {
        id = in.readInt();
        login = in.readString();
        nome = in.readString();
        email = in.readString();
        urlFoto = in.readString();
        token = in.readString();
        roles = in.createStringArray();
    }



    public String[] getRoles() {
        return roles;
    }

    public void setRoles(String[] roles) {
        this.roles = roles;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getUrlFoto() {
        return urlFoto;
    }

    public void setUrlFoto(String urlFoto) {
        this.urlFoto = urlFoto;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }



}
