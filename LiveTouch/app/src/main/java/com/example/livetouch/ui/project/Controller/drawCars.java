package com.example.livetouch.ui.project.Controller;

import android.os.Bundle;
import android.view.View;
import android.view.Menu;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.example.livetouch.R;
;
import com.example.livetouch.ui.project.Controller.home.HomeFragment;
import com.example.livetouch.ui.project.model.AsyncGET;
import com.example.livetouch.ui.project.model.CarroAdapter;
import com.example.livetouch.ui.project.model.Retorno;
import com.example.livetouch.ui.project.model.User;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.navigation.NavigationView;
import com.example.livetouch.ui.project.model.Carro;
import com.squareup.picasso.Picasso;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import java.util.ArrayList;
import java.util.concurrent.ExecutionException;

public class drawCars extends AppCompatActivity {

    private AppBarConfiguration mAppBarConfiguration;
    private TextView tvNameUser;
    private TextView tvMailUser;
    private ImageView profileImg;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_draw_cars);
        Toolbar toolbar = findViewById(R.id.toolbar);
        User usuari = (User)getIntent().getSerializableExtra("usuario");

        AsyncGET asyncGET = new AsyncGET();
        asyncGET.setToken(usuari.getToken());
        try {
            Retorno[] r = asyncGET.execute().get();
            HomeFragment hf = new HomeFragment();
            Bundle bundle = new Bundle();
            bundle.putString("token",usuari.getToken());
            hf.setArguments(bundle);
            }
        catch (ExecutionException e){
            e.printStackTrace();
        }
        catch(InterruptedException e){
            e.printStackTrace();
        }

        //o app quebra sempre que tento executar as imagens por listView :(
//        NavigationView nv2 = (NavigationView)findViewById(R.id.nav_view);
//        View navigationHome = nv2.inflateHeaderView(R.layout.fragment_home);
//        ListView lista = (ListView)navigationHome.findViewById(R.id.lvCarros);
//        ArrayAdapter adapter = new CarroAdapter(this,adicionarCarros());
//        lista.setAdapter(adapter);

        setSupportActionBar(toolbar);

        NavigationView nv = (NavigationView)findViewById(R.id.nav_view);
        View headerview = nv.getHeaderView(0);
        ImageView profileImg = (ImageView)headerview.findViewById(R.id.imgProfile);
        Picasso.with(this).load(usuari.getUrlFoto()).into(profileImg);
        tvNameUser = (TextView) headerview.findViewById(R.id.nameUser);
        tvMailUser = (TextView) headerview.findViewById(R.id.mailUser);

//        Bundle extra = getIntent().getExtras();


        if (usuari != null){
            String nome = usuari.getNome();
            String mail = usuari.getEmail();
            tvNameUser.setText(nome);
            tvMailUser.setText(mail);
        }

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        NavigationView navigationView = findViewById(R.id.nav_view);


        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.
        mAppBarConfiguration = new AppBarConfiguration.Builder(
                R.id.nav_home, R.id.nav_gallery, R.id.nav_slideshow)
                .setDrawerLayout(drawer)
                .build();
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        NavigationUI.setupActionBarWithNavController(this, navController, mAppBarConfiguration);
        NavigationUI.setupWithNavController(navigationView, navController);
    }

    private ArrayList<Carro>adicionarCarros(){
        ArrayList<Carro>carros = new ArrayList<Carro>();

        Carro c = new Carro("gallardo","corrida",R.drawable.ferrari);
        carros.add(c);
         c = new Carro("astra","popular",R.drawable.astra);
        carros.add(c);
         c = new Carro("ferrari","corrida",R.drawable.gallardo);
        carros.add(c);
         c = new Carro("aventador","corrida",R.drawable.bugatti);
        carros.add(c);
         c = new Carro("bugatti","corrida",R.drawable.aventador);
        carros.add(c);

        return carros;
    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.draw_cars, menu);
        return true;
    }

    @Override
    public boolean onSupportNavigateUp() {
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        return NavigationUI.navigateUp(navController, mAppBarConfiguration)
                || super.onSupportNavigateUp();
    }
}
