package com.example.livetouch.ui.project.Controller.slideshow;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

public class SlideshowViewModel extends ViewModel {

    private MutableLiveData<String> mText;

    public SlideshowViewModel() {
        mText = new MutableLiveData<>();
        mText.setValue("Não consegui implementar o Logout, tentei colocar um listener com um comparador do id do item do menu do navigation drawer, mas sem sucesso.");
    }

    public LiveData<String> getText() {
        return mText;
    }
}