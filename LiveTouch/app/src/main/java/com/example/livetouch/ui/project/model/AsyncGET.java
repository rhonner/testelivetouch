package com.example.livetouch.ui.project.model;

import android.os.AsyncTask;

import com.google.gson.Gson;

import java.io.IOException;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class AsyncGET extends AsyncTask<String,Void,Retorno[]> {
    String token;

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    @Override
    protected Retorno[] doInBackground(String... strings) {
        OkHttpClient client = new OkHttpClient();
        Retorno[] r = null;
        MediaType JSON = MediaType.parse("application/json; charset=utf-8");
        Request request = new Request.Builder()
                .url("https://carros-springboot.herokuapp.com/api/v2/carros")
                .addHeader("Authorization","Bearer "+ this.token)
                .build();
        Response response = null;
        try {
            response = client.newCall(request).execute();
            String jresposta = response.body().string();
            Gson gson = new Gson();
            r = gson.fromJson(jresposta, Retorno[].class);
            System.out.println(r);
        }catch(IOException e){
            e.printStackTrace();
        }
        return r;
    }
}
