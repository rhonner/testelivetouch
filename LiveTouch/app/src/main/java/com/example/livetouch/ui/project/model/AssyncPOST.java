package com.example.livetouch.ui.project.model;

import android.os.AsyncTask;

import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class AssyncPOST extends AsyncTask<String,Void,User> {
    String login;

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getSenha() {
        return senha;
    }

    public void setSenha(String senha) {
        this.senha = senha;
    }

    String senha;
    @Override
    protected User doInBackground(String... params) {
        User user = new User();
        OkHttpClient client = new OkHttpClient();
        JSONObject jsonobject = new JSONObject();
        try {
            jsonobject.put("username", this.login);
            jsonobject.put("password",this.senha);
        }catch(JSONException e ){
            e.printStackTrace();
        }

        MediaType JSON = MediaType.parse("application/json; charset=utf-8");
        RequestBody body = RequestBody.create(JSON, jsonobject.toString());
        Request request = new Request.Builder()
                .url("https://carros-springboot.herokuapp.com/api/v2/login")
                .post(body)
                .build();
        Response response = null;
        try {
            response = client.newCall(request).execute();
            String jresposta = response.body().string();
            Gson gson = new Gson();
             user = gson.fromJson(jresposta, User.class);
            System.out.println(user);
        }catch(IOException e){
            e.printStackTrace();
        }

        return user;
    }

    @Override
    protected void onPostExecute(User user) {
        handleObject(user);

    }

    private User handleObject(User u){
        return u;
    }
}
