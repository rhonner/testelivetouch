package com.example.livetouch.ui.project.Controller.home;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.example.livetouch.R;
import com.example.livetouch.ui.project.model.Carro;
import com.example.livetouch.ui.project.model.CarroAdapter;

import java.util.ArrayList;
import java.util.List;

public class HomeFragment extends Fragment {

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        if (savedInstanceState != null){
            System.out.println("mec");
        }
        View view = inflater.inflate(R.layout.fragment_home,container,false);
        ListView carlist = (ListView)view.findViewById(R.id.lvCarros);
        List<Carro> cars= adicionarCarros();
        CarroAdapter ca = new CarroAdapter(this.getActivity(), cars);

        carlist.setAdapter(ca);
        return view;
//        homeViewModel =
//                ViewModelProviders.of(this).get(HomeViewModel.class);
//        View root = inflater.inflate(R.layout.fragment_home, container, false);
//
//
//        homeViewModel.getText().observe(getViewLifecycleOwner(), new Observer<String>() {
//            @Override
//            public void onChanged(@Nullable String s) {
//
//            }
//        });
//
//        return root;
    }

    private List<Carro>adicionarCarros(){
        List<Carro> cars = new ArrayList<Carro>();

        Carro c = new Carro("gallardo","corrida",R.drawable.ferrari);
        cars.add(c);
        c = new Carro("astra","popular",R.drawable.astra);
        cars.add(c);
        c = new Carro("ferrari","corrida",R.drawable.gallardo);
        cars.add(c);
        c = new Carro("aventador","corrida",R.drawable.bugatti);
        cars.add(c);
        c = new Carro("bugatti","corrida",R.drawable.aventador);
        cars.add(c);

        return cars;
    }
}
